package bank;


public class Request {
	public String operation;
	public String name;
	public String param1;

	public Request(String operation, String name, String param1) {
		this.operation = operation;
		this.name = name;
		this.param1 = param1;
	}

	public Request(String operation, String name) {
		this(operation, name, null);
	}

	public Request(String operation) {
		this(operation, null, null);
	}
}
