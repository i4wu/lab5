package bank;

public class ConsumerRules extends AccountRules {

	public ConsumerRules() {
		setName(CONSUMER_RULES);
	}
	
	@Override
	public float addMoney(float balance, float amount) {
		auditLog.addEntry("Account " + name + ": Adding " + amount);
		return balance += amount;
	}

	@Override
	public float removeMoney(float balance, float amount) throws Exception {
		if (balance - amount >= 0) {
			auditLog.addEntry("Account " + name + ": Removing " + amount);
			return balance -= amount;
		} else {
			auditLog.addEntry("Rejected withdrawal of " + amount);
			throw new Exception("Not allowed to overdraw");
		}
	}

}
