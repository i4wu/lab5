package bank;

public class GovernmentRules extends AccountRules {

	public GovernmentRules() {
		setName(GOVERNMENT_RULES);
	}
	
	@Override
	public float addMoney(float balance, float amount) {
		auditLog.addEntry("Account " + name + ": Adding " + amount);
		return balance += amount;
	}

	@Override
	public float removeMoney(float balance, float amount) throws Exception {
		if (balance - amount < 5000) {
			auditLog.addEntry("Rejected withdrawal of " + amount);
			throw new Exception("Not allowed to drop below $5000");
		}
		else {
			auditLog.addEntry("Account " + name + ": Removing " + amount);
			return balance -= amount;
		}
	}

}
