package bank;

import static org.junit.Assert.*;

import org.junit.Test;

public class BankTest {

	@Test
	public void testDispatch() {
		Bank bankTest = new Bank();
		
		//Requests
		Request addRequestC = new Request("addAccount", "nameC", Bank.CONSUMER_RULES);
		Request addRequestB = new Request("addAccount", "nameB", Bank.BUSINESS_RULES);
		
		Request withdrawRequestC = new Request("withdraw", "nameC", "100");
		Request withdrawRequestB = new Request("withdraw", "nameB", "100");
		
		Request depositRequestC = new Request("deposit", "nameC", "1000");
		Request depositRequestB = new Request("deposit", "nameB", "1000");
		
		Request getRequestC = new Request("getStatement", "nameC", "");
		Request getRequestB = new Request("getStatement", "nameB", "");

		
		//adding accounts
		try {
			bankTest.dispatch(addRequestC);
			bankTest.dispatch(addRequestB);
			bankTest.dispatch(addRequestC);
			fail();
		} catch (Exception e) {
			// Do nothing
			//e.printStackTrace();
		}
		assertTrue(bankTest.getAccountBalance().containsKey("nameC"));
		assertTrue(bankTest.getAccountBalance().containsKey("nameB"));
		
		//Withdraw when below
		try {
			bankTest.dispatch(withdrawRequestB);
			fail();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			//e.printStackTrace();
		}
		
		//deposit then withdraw
		try {
			bankTest.dispatch(depositRequestC);
			bankTest.dispatch(depositRequestB);
			
			bankTest.dispatch(withdrawRequestB);
			bankTest.dispatch(withdrawRequestC);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			//e.printStackTrace();
		}
		
		//getStatements
		try {
			assertEquals(bankTest.dispatch(getRequestC), "Customer: nameC, balance: " + (float)(1000-100));
			assertEquals(bankTest.dispatch(getRequestB), "Customer: nameB, balance: " + (float)(990-100));
		} catch (Exception e) {
			// TODO Auto-generated catch block
			//e.printStackTrace();
		}
	}

}
