package bank; 
 
public abstract class AccountRules { 
 
	public static final String CONSUMER_RULES = "Consumer"; 
	public static final String BUSINESS_RULES = "Business"; 
	public static final String GOVERNMENT_RULES = "Government";
	
	protected String name; 
	protected AuditLog auditLog = AuditLog.getInstance(); 
 	public static AccountRules getRules(String rulesType) throws Exception { 
 		if (CONSUMER_RULES.equals(rulesType)) { 
 			return new ConsumerRules(); 
 		} else if (BUSINESS_RULES.equals(rulesType)) { 
 			return new BusinessRules(); 
 		} else if (GOVERNMENT_RULES.equals(rulesType)) {
 			return new GovernmentRules();
 		} else { 
 			throw new Exception("Unknown rule type " + rulesType); 
 		} 
 	} 
 
 	public abstract float addMoney(float balance, float amount); 
 	public abstract float removeMoney(float balance, float amount) throws Exception; 
 
 	public void setName(String name) { 
 		this.name = name; 
 	} 
 
 	protected void addLogEntry(String entry) { 
 		auditLog.addEntry("Account " + name + ": " + entry); 
 	} 
} 
