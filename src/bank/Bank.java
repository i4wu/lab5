package bank;

import java.util.HashMap;
import java.util.Map;

public class Bank {

	private AuditLog auditLog = AuditLog.getInstance();

	public static final String CONSUMER_RULES = "Consumer";
	public static final String BUSINESS_RULES = "Business";
	public static final String GOVERNMENT_RULES = "Government";

	private Map<String, Account> accounts = new HashMap<String, Account>();
	private Map<String, Float> accountBalance = new HashMap<String, Float>();
	private Map<String, String> accountRules = new HashMap<String, String>();

	public Object dispatch(Request request) throws Exception {
		Object returnVal = null;
		if (request.operation.equals("addAccount")) {
			return addAccount(request);
		} else if (request.operation.equals("withdraw")) {
			withdraw(request);
		} else if (request.operation.equals("deposit")) {
			deposit(request);
		} else if (request.operation.equals("getStatement")) {
			return getStatement(request);
		} else if (request.operation.equals("getLog")) {
			return auditLog.getLog();
		} else {
			throw (new Exception("Unknown request " + request.operation));
		}
		return returnVal;
	}

	private Object getStatement(Request request) {
		String name = request.name;
		float balance = accounts.get(name).getBalance();
		return "Customer: " + name + ", balance: " + balance;
	}

	private void deposit(Request request) throws Exception {
		Account account = accounts.get(request.name);
		if (account == null) {
			throw new Exception("Unknown account " + request.name);
		}
		
		account.deposit(Float.parseFloat(request.param1));
	}

	private void withdraw(Request request) throws Exception {
		Account account = accounts.get(request.name);
		if (account == null) {
			throw new Exception("Unknown account " + request.name);
		}
		
		account.withdraw(Float.parseFloat(request.param1));
	}

	private Object addAccount(Request request) throws Exception {
		if (accountRules.containsKey(request.name)) {
			throw new Exception("Account " + request.name
					+ " already exists");
		}
		if (CONSUMER_RULES.equals(request.param1)
				|| BUSINESS_RULES.equals(request.param1)
				|| GOVERNMENT_RULES.equals(request.param1)) {
			accounts.put(request.name, new Account(request.name, request.param1));
			auditLog.addEntry("Adding account for " + request.name);
			return null;
		} else {
			throw new Exception("Unknown rule type " + request.param1);
		}
	}

	public static void main(String[] args) {
		Bank bank = new Bank();
		try {
			bank.dispatch(new Request("addAccount", "Barry", 
					 AccountRules.CONSUMER_RULES)); 
			bank.dispatch(new Request("addAccount", "Fedex", 
					 AccountRules.BUSINESS_RULES)); 
			bank.dispatch(new Request("addAccount", "UCSD", 
					 AccountRules.GOVERNMENT_RULES)); 
			bank.dispatch(new Request("deposit", "Barry", "200")); 
			bank.dispatch(new Request("withdraw", "Barry", "50")); 
			bank.dispatch(new Request("getStatement", "Barry")); 
			bank.dispatch(new Request("withdraw", "Barry", "149")); 
			bank.dispatch(new Request("getStatement", "Barry")); 
			bank.dispatch(new Request("deposit", "Fedex", "2000")); 
			bank.dispatch(new Request("withdraw", "Fedex", "200")); 
			bank.dispatch(new Request("getStatement", "Fedex"));
		} catch (Exception e) {
			System.out.println("Exception: " + e);
		}
	}

	public Map<String, Float> getAccountBalance() {
		return accountBalance;
	}

	public void setAccountBalance(Map<String, Float> accountBalance) {
		this.accountBalance = accountBalance;
	}
}
