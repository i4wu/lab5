package bank;

public class Account {
	private String name;
	private float balance;
	private String rule;
	
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public float getBalance() {
		return balance;
	}

	public void setBalance(float balance) {
		this.balance = balance;
	}

	public String getRule() {
		return rule;
	}

	public void setRule(String rule) {
		this.rule = rule;
	}

	
	public Account(String name, String rule) {
		setName(name);
		setRule(rule);
		balance = 0.0F;
	}
	
	//deposits money and returns auditLog String
	public void deposit(float amount) throws Exception {
		AccountRules rules = AccountRules.getRules(getRule());
		setBalance(rules.addMoney(getBalance(), amount));
	}
	
	public void withdraw(float amount) throws Exception {
		setBalance(AccountRules.getRules(getRule()).removeMoney(getBalance(), amount));
	}
	
	public String getStatement() {
		float balance = getBalance();
		return "Customer: " + name + ", balance: " + balance;
	}
}
